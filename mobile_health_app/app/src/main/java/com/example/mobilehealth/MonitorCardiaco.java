package com.example.mobilehealth;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.anychart.anychart.AnyChart;
import com.anychart.anychart.AnyChartView;
import com.anychart.anychart.Cartesian;
import com.anychart.anychart.DataEntry;
import com.anychart.anychart.TooltipPositionMode;
import com.anychart.anychart.ValueDataEntry;
import com.example.mobilehealth.constante.Constantes;
import com.example.mobilehealth.data.DataUtils;
import com.example.mobilehealth.data.GeradorBatimento;
import com.example.mobilehealth.data.Preference;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MonitorCardiaco extends AppCompatActivity {

    private Preference preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monitor_cardiaco);
        preferences = new Preference(this);

        avisoBluetooth();
        carregarGraficoDiario();
        verificarGraficosPassado();

        CircleImageView imageFotoPerfil = findViewById(R.id.imageFotoPerfil);
        carregarImagem();
        findViewById(R.id.imageEdit).setVisibility(View.INVISIBLE);
        findViewById(R.id.progressBar).setVisibility(View.INVISIBLE);


    }

    public void sincronizarGraficoDiario(View v) {
        if (isAtivoBluetooth()) {
            gerarGraficoDiario();
            com.anychart.anychart.AnyChartView grafico = findViewById(R.id.any_chart_view);
            grafico.setVisibility(View.VISIBLE);
            TextView dataAtualizacao = findViewById(R.id.dataAtualizacao);
            String data = DataUtils.dataStamp();
            dataAtualizacao.setText("Última atualização: " + data);
            dataAtualizacao.setVisibility(View.VISIBLE);
            preferences.storeString(Constantes.INDICADOR_MONITOR_CARDIACO_DT_ATUALIZACAO, data);

        } else {
            avisoBluetooth();
        }
    }

    public void verificarGraficosPassado() {
        if (preferences.getStoredString(Constantes.INDICADOR_MONITOR_CARDIACO_DT_ATUALIZACAO).length() > 0) {
            TextView dataAtualizacao = findViewById(R.id.dataAtualizacao);
            dataAtualizacao.setText("Última atualização: " + preferences.getStoredString(Constantes.INDICADOR_MONITOR_CARDIACO_DT_ATUALIZACAO));
            dataAtualizacao.setVisibility(View.VISIBLE);
            com.anychart.anychart.AnyChartView grafico = findViewById(R.id.any_chart_view);
            grafico.setVisibility(View.VISIBLE);
        }
    }

    public void carregarGraficoDiario() {
        Cartesian cartesian = AnyChart.line();

        cartesian.getTooltip().setPositionMode(TooltipPositionMode.POINT);
        cartesian.setAnimation(true);

        cartesian.setYAxis("Batimentos");
        cartesian.getXAxis().getLabels().setPadding(5d, 5d, 5d, 5d);

        List<String> batimentos = null;
        if (preferences.getStoredString(Constantes.INDICADOR_MONITOR_CARDIACO_DT_ATUALIZACAO).length() == 0) {
            batimentos = GeradorBatimento.gerarBatimentoDiario();
            for (int i = 0; i < batimentos.size(); i++) {
                preferences.storeString(Constantes.DADOS_MONITOR_CARDIACO + "_"+i, batimentos.get(i));
            }
        }
        batimentos = capturarDadosGraficoMOnitor();

        List<DataEntry> seriesData = new ArrayList<>();
        for (int i = 0; i < batimentos.size(); i++) {
            seriesData.add(new ValueDataEntry(String.valueOf(i) + "h", Integer.valueOf(batimentos.get(i))));
        }

        Cartesian series1 = cartesian.setData(seriesData);

        AnyChartView anyChartView = findViewById(R.id.any_chart_view);
        anyChartView.setChart(cartesian);
    }

    public void gerarGraficoDiario() {
        AnyChartView anyChartView = (AnyChartView) findViewById(R.id.any_chart_view);

        Cartesian cartesian = AnyChart.line();
        cartesian.getTooltip().setPositionMode(TooltipPositionMode.POINT);
        cartesian.setAnimation(true);

        cartesian.setYAxis("Batimentos");
        cartesian.getXAxis().getLabels().setPadding(5d, 5d, 5d, 5d);

        List<String> batimentos = null;
        batimentos = GeradorBatimento.gerarBatimentoDiario();
        for (int i = 0; i < batimentos.size(); i++) {
           preferences.storeString(Constantes.DADOS_MONITOR_CARDIACO + "_"+i, batimentos.get(i));
        }

        batimentos = capturarDadosGraficoMOnitor();

        List<DataEntry> seriesData = new ArrayList<>();
        for (int i = 0; i < batimentos.size(); i++) {
            seriesData.add(new ValueDataEntry(String.valueOf(i) + "h", Integer.valueOf(batimentos.get(i))));
        }

        Cartesian series1 = cartesian.setData(seriesData);
        anyChartView.setChart(cartesian);

        //Reinicia e recarrega o gráfico
        finish();
        startActivity(new Intent(this, MonitorCardiaco.class));
        overridePendingTransition(0, 0);
    }

    public List<String> capturarDadosGraficoMOnitor() {
        List<String> batimentos = new ArrayList<>();
        Log.i("Entrou Horas", "");
        for (int i = 0; i < 24; i++) {
            batimentos.add(preferences.getStoredString(Constantes.DADOS_MONITOR_CARDIACO + "_"+i));
        }
        return batimentos;
    }

    public void avisoBluetooth() {
        if (!isAtivoBluetooth()) {
            AlertDialog.Builder a = new AlertDialog.Builder(this);
            a.setTitle("Bluetooh desativado");
            a.setMessage("Ative o Bluetooth e faça o pareamento com a smartband.");
            a.show();
        }
    }

    public boolean isAtivoBluetooth() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter.isEnabled()) {
            return true;
        }
        return false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    public void carregarImagem() {
        if (preferences.getStoredString(Constantes.IMAGE_PERFIL).length() > 0) {
            Uri uri = Uri.fromFile(new File(preferences.getStoredString(Constantes.IMAGE_PERFIL)));
            CircleImageView imageFotoPerfil = findViewById(R.id.imageFotoPerfil);
            imageFotoPerfil.setImageURI(uri);
        }
    }

    public void abrirComunidade(View v){
        startActivity(new Intent(this, PaginaComunidade.class));
    }
    public void menuHome(View v){
        startActivity(new Intent(this, Home.class));
    }
}
