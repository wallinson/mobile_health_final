package com.example.mobilehealth;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class AdaptadorExercicio extends BaseAdapter {

    private final List<List<String>> listaExercicio;
    private final Activity act;

    public AdaptadorExercicio(List<List<String>> listaExercicio, Activity act) {
        this.listaExercicio = listaExercicio;
        this.act = act;
    }

    @Override
    public int getCount() {
        return listaExercicio.size();
    }

    @Override
    public List<String> getItem(int position) {
        return listaExercicio.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = act.getLayoutInflater().inflate(R.layout.list_view_exercicio, parent, false);
        List<String> exercicio = listaExercicio.get(position);

        TextView vExercicio = view.findViewById(R.id.nomeExercicio);
        TextView vCaloria = view.findViewById(R.id.caloriaExercicio);
        TextView vTempo = view.findViewById(R.id.tempoExercicio);

        vExercicio.setText(exercicio.get(0));
        vCaloria.setText(exercicio.get(1)+" min");
        vTempo.setText(exercicio.get(2)+" cal");


        return view;
    }
}
