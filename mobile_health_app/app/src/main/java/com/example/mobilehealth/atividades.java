package com.example.mobilehealth;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mobilehealth.constante.Constantes;
import com.example.mobilehealth.data.Preference;

import java.util.ArrayList;
import java.util.List;

public class atividades extends AppCompatActivity {

    private Preference preferences;
    private ViewHolder holder = new ViewHolder();

    AdaptadorExercicio adapter;

    final List<List<String>> listaExercicio=new ArrayList<List<String>>();
    final List<String> exercicio =new ArrayList<String>();
    final List<String> caloria =new ArrayList<String>();
    final List<String> tempo =new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atividades);
        preferences = new Preference(this);

        holder.exercicio = findViewById(R.id.spinnerExercicio);
        holder.tempoExercicio = findViewById(R.id.inputTempoExercicio);
        holder.addExercicio = findViewById(R.id.addExercicio);
        holder.listViewAtividades = findViewById(R.id.listViewAtividades);

        //DEFINING A STRING ADAPTER WHICH WILL HANDLE THE DATA OF THE LISTVIEW
        adapter = new AdaptadorExercicio( listaExercicio,this);
        holder.listViewAtividades.setAdapter(adapter);

        holder.addExercicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.exercicio.getSelectedItemPosition()>0 && holder.tempoExercicio.getText().length() > 0){
                    if(!listaExercicio.contains(holder.exercicio.getSelectedItem())){
                        String tempoExercicio = holder.tempoExercicio.getText().toString();
                        ArrayList<String> temp = new ArrayList<>();
                        temp.add( holder.exercicio.getSelectedItem().toString());
                        temp.add(tempoExercicio);
                        temp.add(String.valueOf(Integer.valueOf(tempoExercicio)*10));
                        listaExercicio.add(temp);
                        exercicio.add(holder.exercicio.getSelectedItem().toString());
                        caloria.add(String.valueOf(Integer.valueOf(tempoExercicio)*10));
                        tempo.add(tempoExercicio);
                        adapter.notifyDataSetChanged();
                        holder.exercicio.setSelection(0);
                        holder.tempoExercicio.setText("");
                    }else{
                        Toast.makeText(atividades.this, "O tipo de exercício já foi cadastrado!!!", Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(atividades.this, "Selecione uma execício válido e insira a duração!!!", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private static class ViewHolder {
        Spinner exercicio;
        EditText tempoExercicio;
        Button addExercicio;
        ListView listViewAtividades;
    }

    @Override
    protected void onPause() {
        super.onPause();
        preferences.storeListString(Constantes.LISTA_EXERCICIO, exercicio);
        preferences.storeListString(Constantes.LISTA_EXERCICIO_CALORIA, caloria);
        preferences.storeListString(Constantes.LISTA_EXERCICIO_TEMPO, tempo);
        finish();
    }
}
