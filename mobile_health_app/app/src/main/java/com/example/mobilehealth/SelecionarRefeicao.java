package com.example.mobilehealth;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class SelecionarRefeicao extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selecionar_refeicao);
    }

    public void abrirComunidade(View v){
        startActivity(new Intent(this, PaginaComunidade.class));
    }
    public void menuHome(View v){
        startActivity(new Intent(this, Home.class));
    }
}
