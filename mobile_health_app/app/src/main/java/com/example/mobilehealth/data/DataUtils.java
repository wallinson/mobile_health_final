package com.example.mobilehealth.data;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DataUtils {

    public static String dataStamp(){
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss dd/MM/YYYY");
        Calendar c = Calendar.getInstance();
        return sdf.format(c.getTime());
    }
}
