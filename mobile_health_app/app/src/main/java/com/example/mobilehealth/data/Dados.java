package com.example.mobilehealth.data;

import com.anychart.anychart.DataEntry;

public class Dados extends DataEntry {

    public Dados(String x, Number [] value) {
        setValue("x", x);
        setValue("value", value);
    }

    public Dados(String x, String category, Number [] value) {
        setValue("x", x);
        setValue("category", category);
        setValue("value", value);
    }
}
