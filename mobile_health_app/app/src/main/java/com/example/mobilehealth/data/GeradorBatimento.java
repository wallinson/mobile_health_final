package com.example.mobilehealth.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GeradorBatimento {


    public static  List<String> gerarBatimentoDiario(){
        List<String> lista = new ArrayList<>();
        int base = 100;
        Random r = new Random();
        int batimento = 0;
        for (int i = 0; i < 24 ; i++){
            if (i<6){
                batimento = r.nextInt(5)+(base-5);
            }else if (i>=6 && i<12){
                batimento = r.nextInt(10)+base;
            }else if (i>=12 && i<=20){
                batimento = r.nextInt(20)+base;
            }else{
                batimento = r.nextInt(5)+(base-5);
            }
            lista.add(String.valueOf(batimento));
        }
       return lista;
    }

}
