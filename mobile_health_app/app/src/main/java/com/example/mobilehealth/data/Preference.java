package com.example.mobilehealth.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.mobilehealth.constante.Constantes;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Preference {

    private SharedPreferences preferences;
    private String nomePreference = "MobileHealth";

    public Preference (Context context){
        this.preferences = context.getSharedPreferences(nomePreference, Context.MODE_PRIVATE);
    }

    public void storeString(String key, String value){
        this.preferences.edit().putString(key, value).apply();
    }

    public void storeListString(String key, List<String> value) {
        List<String> listaAntiga = this.getStoreListString(key);
        listaAntiga.addAll(value);

        if (!listaAntiga.isEmpty()) {
            String temp = "";
            for (String s : listaAntiga) {
                temp += "-" + s;
            }
            this.preferences.edit().putString(key, temp).apply();
        }
    }

    public List<String> getStoreListString(String key) {
        List<String> value = new ArrayList<>();
        String[] temp = this.preferences.getString(key, "").split("-");
        for (String s : temp) {
            value.add(s);
        }
        return value;
    }

    public String ultimoTempoExercicio() {
        List<String> storeListString = getStoreListString(Constantes.LISTA_EXERCICIO_TEMPO);
        if (storeListString.isEmpty() || storeListString.size()<=1) {
            return "0";
        } else {
            return storeListString.get(storeListString.size() - 1);
        }
    }

    public String ultimaCaloriaGasta() {
        List<String> storeListString = getStoreListString(Constantes.LISTA_EXERCICIO_CALORIA);
        if (storeListString.isEmpty() || storeListString.size()<=1) {
            return "0";
        } else {
            return storeListString.get(storeListString.size() - 1);
        }
    }

    public int somaCaloriaGasta() {
        List<String> storeListString = getStoreListString(Constantes.LISTA_EXERCICIO_CALORIA);
        int caloria = 0;

        if (storeListString.size() > 1) {
            for (String s : storeListString) {
                if (!s.equals("")) {
                    caloria += Integer.parseInt(s);
                }
            }
        }
        return caloria;
    }

    public void storesSetString(String key, Set value){
        this.preferences.edit().putStringSet(key, value).apply();
    }

    public Set<String> getStoresSetString(String key){
      return  this.preferences.getStringSet(key, new HashSet<String>());
    }

    public void storeBoolean(String key, Boolean value){
        this.preferences.edit().putBoolean(Constantes.INDICADORES_CONFIGURADOS,true).apply();
        this.preferences.edit().putBoolean(key, value).apply();
    }

    public boolean getStoredBoolean(String key){
        return this.preferences.getBoolean(key, false);
    }

    public String getStoredString(String key){
        return this.preferences.getString(key, "");
    }

    public void atualizarNivel(){
        if(somaCaloriaGasta()>=Integer.valueOf(this.preferences.getString(Constantes.PROGRESSO_MAX, ""))){
            int max = Integer.valueOf(this.preferences.getString(Constantes.PROGRESSO_MAX, ""));
            int calorias = somaCaloriaGasta();
            String level = getStoredString(Constantes.PROGRESSO_LEVEL).replace("Lv.", "");
            int levelAtual = Integer.valueOf(level);

            while(max<calorias){
                max  = (int) (max * 1.30);
                levelAtual++;
            }

            storeString(Constantes.PROGRESSO_LEVEL, "Lv."+levelAtual);
            storeString(Constantes.PROGRESSO_MAX,""+Integer.valueOf(max));
        }
    }

    public boolean isEmailCadastrado(String value){
        String usuario = this.preferences.getString(Constantes.EMAIL_USUARIO, "");
        return usuario != null;
    }

    public boolean isHomeConfigurado(){
        return this.preferences.getBoolean(Constantes.INDICADORES_CONFIGURADOS, false);
    }

    public boolean isLogado(){
        return this.preferences.getBoolean(Constantes.SESSAO,new Boolean(false));
    }

    public int quantidadePostagem(){
        return Integer.parseInt(getStoredString(Constantes.QUANTIDADE_POST));
    }

    public void inicializarContadorPostagem(){
        storeString(Constantes.QUANTIDADE_POST,""+String.valueOf(0));
    }

    private void incrementarPostagem(){
        storeString(Constantes.QUANTIDADE_POST,""+String.valueOf(quantidadePostagem()+1));
    }

    public void cadastrarPostagem( Set postagem){
        incrementarPostagem();
        storesSetString(Constantes.INDICE_POSTAGEM+"_"+quantidadePostagem(),postagem);
    }

    public List<Set> listarPostagens(){
        List<Set> postagem = new ArrayList<>();
        for (int i = quantidadePostagem() ; i > 0; i--) {
            postagem.add(getStoresSetString(Constantes.INDICE_POSTAGEM+"_"+i));
        }
        return postagem;
    }


}
