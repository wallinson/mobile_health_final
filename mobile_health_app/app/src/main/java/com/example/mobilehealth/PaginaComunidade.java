package com.example.mobilehealth;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.mobilehealth.data.Preference;

import java.util.List;
import java.util.Set;

public class PaginaComunidade extends AppCompatActivity {

    private Preference preferences;
    private ViewHolder holder;
    private AdaptadorPostagens adapter;
    private List<Set> listaPostagens;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pagina_comunidade);
        preferences = new Preference(this);
        holder = new ViewHolder();

        holder.fotoPerfil = findViewById(R.id.imageFotoPerfil);
        holder.nomePerfil = findViewById(R.id.textNomeUsuario);
        holder.progressBar = findViewById(R.id.progressBar);
        holder.level = findViewById(R.id.textLevel);
        holder.listaPostagem = findViewById(R.id.listaPostagem);

        listaPostagens = preferences.listarPostagens();
        adapter = new AdaptadorPostagens(listaPostagens, this);
        holder.listaPostagem.setAdapter(adapter);

        ActionBar supportActionBar = getSupportActionBar();
        supportActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#F80C0C")));
        supportActionBar.setTitle("Health Community");


    }

    public void criarPostagem(View v){
        startActivity(new Intent(this, Postagem.class));}

    public class ViewHolder{
        ImageView fotoPerfil;
        TextView nomePerfil;
        ProgressBar progressBar;
        TextView level;
        ListView listaPostagem;
    }

    public void abrirComunidade(View v){
        startActivity(new Intent(this, PaginaComunidade.class));
    }
    public void menuHome(View v){
        startActivity(new Intent(this, Home.class));
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
