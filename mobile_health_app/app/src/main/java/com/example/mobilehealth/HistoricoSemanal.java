package com.example.mobilehealth;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.anychart.anychart.AnyChart;
import com.anychart.anychart.AnyChartView;
import com.anychart.anychart.DataEntry;
import com.anychart.anychart.Pie;
import com.anychart.anychart.ValueDataEntry;
import com.example.mobilehealth.constante.Constantes;
import com.example.mobilehealth.data.Preference;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class HistoricoSemanal extends AppCompatActivity {

    private Preference preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historico_semanal);
        preferences = new Preference(this);

        Pie pie = AnyChart.pie();


        List<DataEntry> data = new ArrayList<>();
        data.add(new ValueDataEntry("Semana 1", 2700));
        data.add(new ValueDataEntry("Semana 2", 3200));
        data.add(new ValueDataEntry("Semana 3", 1500));
        data.add(new ValueDataEntry("Semana Atual", preferences.somaCaloriaGasta()));

        pie.setData(data);

        AnyChartView anyChartView = (AnyChartView) findViewById(R.id.any_chart_view_hist);
        anyChartView.setChart(pie);

        CircleImageView imageFotoPerfil = findViewById(R.id.imageFotoPerfil);
        carregarImagem();
        findViewById(R.id.imageEdit).setVisibility(View.INVISIBLE);
        findViewById(R.id.progressBar).setVisibility(View.INVISIBLE);

    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    public void abrirMeuNivel(View v){
        startActivity(new Intent(this, MeuNivel.class));
    }

    public void carregarImagem() {
        if (preferences.getStoredString(Constantes.IMAGE_PERFIL).length() > 0) {
            Uri uri = Uri.fromFile(new File(preferences.getStoredString(Constantes.IMAGE_PERFIL)));
            CircleImageView imageFotoPerfil = findViewById(R.id.imageFotoPerfil);
            imageFotoPerfil.setImageURI(uri);
        }
    }

    public void abrirComunidade(View v){
        startActivity(new Intent(this, PaginaComunidade.class));
    }
    public void menuHome(View v){
        startActivity(new Intent(this, Home.class));
    }
}
