package com.example.mobilehealth;

import android.app.Activity;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mobilehealth.constante.Constantes;
import com.example.mobilehealth.data.Preference;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class AdaptadorPostagens extends BaseAdapter {

    private List<Set> listaPostagem;
    private Activity act;
    private Preference preference;

    public AdaptadorPostagens(List<Set> listaPostagem, Activity act){
        this.act = act;
        this.listaPostagem = listaPostagem;
        preference = new Preference(act);
    }

    @Override
    public int getCount() {
        return listaPostagem.size();
    }

    @Override
    public Set getItem(int position) {
        return listaPostagem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = act.getLayoutInflater().inflate(R.layout.list_view_postagem, parent, false);
        Set postagem = listaPostagem.get(position);

        TextView vNomeUsuario = view.findViewById(R.id.nomePublicador);
        TextView vDescricaoPostagem = view.findViewById(R.id.descricaoPostagem);
        ImageView vImagePerfil = view.findViewById(R.id.imagemPerfil);
        ImageView vPostagem = view.findViewById(R.id.imagePostagem);

        vNomeUsuario.setText(preference.getStoredString(Constantes.NOME_USUARIO));
        vImagePerfil.setImageURI(Uri.fromFile(new File(preference.getStoredString(Constantes.IMAGE_PERFIL))));

        boolean temImagem = false;
        Iterator iterator = postagem.iterator();
        while (iterator.hasNext()){
            String temp = (String) iterator.next();
            if(temp!=null) {
                if(temp.contains("/") || temp.contains("\\")){
                    Uri uri = Uri.fromFile(new File(temp));
                    vPostagem.setImageURI(uri);
                    temImagem=true;
                }else{
                    vDescricaoPostagem.setText(temp);
                }
            }
        }

        if(!temImagem) {
            vPostagem.getLayoutParams().height = 1;
            vPostagem.getLayoutParams().width = 1;
        }

        return view;
    }
}
