package com.example.mobilehealth;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mobilehealth.constante.Constantes;
import com.example.mobilehealth.data.Preference;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;

public class ConfigHome extends AppCompatActivity {

    private Preference preferences;
    private ViewHolder holder = new ViewHolder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config_home);
        preferences = new Preference(this);

        holder.switchRefeicao = findViewById(R.id.switchRefeicao);
        holder.switchExercicio = findViewById(R.id.switchCaloriaGasta);
        holder.switchHistSemanal = findViewById(R.id.switchHistoricoSemanal);
        holder.switchMonitorCardiaco = findViewById(R.id.switchMonitorCardiacao);

        configurarTela();

        holder.switchMonitorCardiaco.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                preferences.storeBoolean(Constantes.INDICADOR_MONITOR_CARDIACO, isChecked);
                if(isChecked){
                    preferences.storeBoolean(Constantes.INDICADOR_HISTORICO_SEMANAL, false);
                    holder.switchHistSemanal.setChecked(false);
                }
            }
        });
        holder.switchHistSemanal.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                preferences.storeBoolean(Constantes.INDICADOR_HISTORICO_SEMANAL, isChecked);
                if(isChecked){
                    preferences.storeBoolean(Constantes.INDICADOR_MONITOR_CARDIACO, false);
                    holder.switchMonitorCardiaco.setChecked(false);
                }
            }
        });
        holder.switchExercicio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                preferences.storeBoolean(Constantes.INDICADOR_EXERCICIO, isChecked);
            }
        });
        holder.switchRefeicao.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                preferences.storeBoolean(Constantes.INDICADOR_REFEICAO, isChecked);
            }
        });

    }

    public void configurarTela() {
        CircleImageView imageFotoPerfil = findViewById(R.id.imageFotoPerfil);
        carregarImagem();
        findViewById(R.id.imageEdit).setVisibility(View.INVISIBLE);
        findViewById(R.id.progressBar).setVisibility(View.INVISIBLE);

        if (preferences.isHomeConfigurado()) {
            holder.switchRefeicao.setChecked(preferences.getStoredBoolean(Constantes.INDICADOR_REFEICAO));
            holder.switchExercicio.setChecked(preferences.getStoredBoolean(Constantes.INDICADOR_EXERCICIO));
            holder.switchHistSemanal.setChecked(preferences.getStoredBoolean(Constantes.INDICADOR_HISTORICO_SEMANAL));
            holder.switchMonitorCardiaco.setChecked(preferences.getStoredBoolean(Constantes.INDICADOR_MONITOR_CARDIACO));
        }
    }

    private static class ViewHolder {
        Switch switchRefeicao;
        Switch switchExercicio;
        Switch switchHistSemanal;
        Switch switchMonitorCardiaco;
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    public void carregarImagem() {
        if (preferences.getStoredString(Constantes.IMAGE_PERFIL).length() > 0) {
            Uri uri = Uri.fromFile(new File(preferences.getStoredString(Constantes.IMAGE_PERFIL)));
            CircleImageView imageFotoPerfil = findViewById(R.id.imageFotoPerfil);
            imageFotoPerfil.setImageURI(uri);
        }
    }
}
