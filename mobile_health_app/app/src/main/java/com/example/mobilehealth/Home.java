package com.example.mobilehealth;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.anychart.anychart.AnyChart;
import com.anychart.anychart.AnyChartView;
import com.anychart.anychart.Cartesian;
import com.anychart.anychart.DataEntry;
import com.anychart.anychart.Pie;
import com.anychart.anychart.ValueDataEntry;
import com.example.mobilehealth.constante.Constantes;
import com.example.mobilehealth.data.GeradorBatimento;
import com.example.mobilehealth.data.Preference;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class Home extends AppCompatActivity {

    private Preference preferences;
    private ViewHolder holder = new ViewHolder();
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_home);
        preferences = new Preference(this);

        holder.nomeUsuario = findViewById(R.id.textNomeUsuario);
        holder.tempoExercicio = findViewById(R.id.textTempoExercicio);
        holder.quantidadeCaloriaPerdida = findViewById(R.id.textCaloriaGasta);
        holder.quantidadeCaloriaIngerida = findViewById(R.id.textCaloriaIngerida);
        holder.botaoAddRefeicao = findViewById(R.id.buttonAddRefeicao);
        holder.imageFotoPerfil = findViewById(R.id.imageFotoPerfil);
        holder.itemExercicio = findViewById(R.id.itemExercicio);
        holder.itemRefeicao = findViewById(R.id.itemRefeicao);
        holder.itemHistSemanal = findViewById(R.id.itemHistoricoSemana);
        holder.itemMonitorCardiaco = findViewById(R.id.itemMonitorCardiacao);
        holder.monitorCardiaco = findViewById(R.id.any_chart_view_home);
        holder.toolbarHome = findViewById(R.id.toolbarHome);
        holder.toolbarDieta = findViewById(R.id.toolbarDieta);
        holder.toolbarExercicio = findViewById(R.id.toolbarExercicio);
        holder.toolbarRefeicao = findViewById(R.id.toolbarComunidade);
        holder.progressBar = findViewById(R.id.progressBar);
        holder.textLevel = findViewById(R.id.textLevel);

        holder.monitorCardiaco.setVisibility(View.INVISIBLE);
        holder.itemHistSemanal.setVisibility(View.INVISIBLE);


        verificarSessao();
        carregarGraficoHistSemanal();
        carregarEventosMenu();
        carregarGraficoMonitorCardiacoDiario();
        verificarGraficoMonitorCardiaco();


    }

    public void criarPostagem(View v){
        startActivity(new Intent(this, Postagem.class));
    }


    public void verificarSessao() {
        if (!preferences.isLogado()) {
            Intent telaLogin = new Intent(this, MainActivity.class);
            startActivity(telaLogin);
            finish();
        } else {
            carregarImagem();
            configurarTela();
            holder.nomeUsuario.setText(preferences.getStoredString(Constantes.NOME_USUARIO));
        }
    }

    public void configurarTela(){
        if(preferences.isHomeConfigurado()) {
            preferences.atualizarNivel();
            holder.itemExercicio.setVisibility(booleanParseInt(preferences.getStoredBoolean(Constantes.INDICADOR_EXERCICIO)));
            holder.itemRefeicao.setVisibility(booleanParseInt(preferences.getStoredBoolean(Constantes.INDICADOR_REFEICAO)));
            holder.itemHistSemanal.setVisibility(booleanParseInt(preferences.getStoredBoolean(Constantes.INDICADOR_HISTORICO_SEMANAL)));
            holder.itemMonitorCardiaco.setVisibility(booleanParseInt(preferences.getStoredBoolean(Constantes.INDICADOR_MONITOR_CARDIACO)));
            holder.progressBar.setMax(Integer.valueOf(preferences.getStoredString(Constantes.PROGRESSO_MAX)));
        }
    }

    public void carregarGraficoMonitorCardiacoDiario() {
        Cartesian cartesian = AnyChart.line();

        cartesian.setYAxis("Batimentos");
        cartesian.getXAxis().getLabels().setPadding(5d, 5d, 5d, 5d);

        List<String> batimentos = null;
        if (preferences.getStoredString(Constantes.INDICADOR_MONITOR_CARDIACO_DT_ATUALIZACAO).length() == 0) {
            batimentos = GeradorBatimento.gerarBatimentoDiario();
            for (int i = 0; i < batimentos.size(); i++) {
                preferences.storeString(Constantes.DADOS_MONITOR_CARDIACO + "_"+i, batimentos.get(i));
            }
        }
        batimentos = capturarDadosGraficoMOnitor();

        List<DataEntry> seriesData = new ArrayList<>();
        for (int i = 0; i < batimentos.size(); i++) {
            seriesData.add(new ValueDataEntry(String.valueOf(i) + "h", Integer.valueOf(batimentos.get(i))));
        }

        Cartesian series1 = cartesian.setData(seriesData);

        AnyChartView anyChartView = (AnyChartView) findViewById(R.id.any_chart_view_home);

        if(preferences.getStoredString(Constantes.INDICADOR_MONITOR_CARDIACO_DT_ATUALIZACAO).length()>1){
            anyChartView.setVisibility(View.VISIBLE);
        }
        anyChartView.setChart(cartesian);
    }

    public List<String> capturarDadosGraficoMOnitor() {
        List<String> batimentos = new ArrayList<>();
        for (int i = 0; i < 24; i++) {
            batimentos.add(preferences.getStoredString(Constantes.DADOS_MONITOR_CARDIACO + "_"+i));
        }
        return batimentos;
    }

    public void verificarGraficoMonitorCardiaco() {
        if ( holder.itemMonitorCardiaco.getVisibility() == View.VISIBLE ) {
            AnyChartView grafico = findViewById(R.id.any_chart_view_home);
            grafico.setVisibility(View.VISIBLE);
        }
    }

    public void carregarImagem() {
        if (preferences.getStoredString(Constantes.IMAGE_PERFIL).length() > 0) {
            Uri uri = Uri.fromFile(new File(preferences.getStoredString(Constantes.IMAGE_PERFIL)));
            holder.imageFotoPerfil.setImageURI(uri);
        }
    }

    public void alterarImagem(View v) {
        dialog = new Dialog(Home.this);
        dialog.setContentView(R.layout.modal_selecionar_imagem);
        dialog.setTitle("Selecione o dispositivo");
        dialog.show();
    }

    public void selecionarImagemCamera(View v) {

    }

    public void selecionarImagemGaleria(View v) {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(Intent.createChooser(i, "Selecione uma imagem"), 123);
        dialog.hide();
    }

    public void configurarHome(View v){
        Intent configHome = new Intent(this,ConfigHome.class);
        startActivity(configHome);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 123) {
                Uri imagemSelecionada = data.getData();
                preferences.storeString(Constantes.IMAGE_PERFIL, getImagePath(imagemSelecionada));
                holder.imageFotoPerfil.setImageURI(imagemSelecionada);
                Log.i("Imagem", imagemSelecionada.toString());
            }
        }
    }

    public void abrirIntentMonitorCardiaco(View v){
        Intent intent = new Intent(this, MonitorCardiaco.class);
        startActivity(intent);
    }

    public void abrirIntentHistSemanal(View v){
        Intent intent = new Intent(this, HistoricoSemanal.class);
        startActivity(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!preferences.isLogado()) {
            finish();
        }
    }

    @Override
    protected void onResume() {
        configurarTela();
        atualizarIndicadorExercicio();
        carregarGraficoHistSemanal();
        super.onResume();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
       /* finish();
        startActivity(new Intent(this, Home.class));
        overridePendingTransition(0, 0);*/
    }

    private static class ViewHolder {
        TextView nomeUsuario;
        TextView tempoExercicio;
        TextView quantidadeCaloriaPerdida;
        TextView quantidadeCaloriaIngerida;
        Button botaoAddRefeicao;
        CircleImageView imageFotoPerfil;

        LinearLayout listaItens;
        RelativeLayout itemExercicio;
        RelativeLayout itemRefeicao;
        RelativeLayout itemHistSemanal;
        RelativeLayout itemMonitorCardiaco;

        AnyChartView monitorCardiaco;
        ImageView toolbarHome;
        ImageView toolbarDieta;
        ImageView toolbarRefeicao;
        ImageView toolbarExercicio;

        ProgressBar progressBar;
        TextView textLevel;
    }

    public void carregarEventosMenu(){
        holder.toolbarHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(Home.this, Home.class));
                overridePendingTransition(0, 0);
            }
        });
        holder.toolbarDieta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Home.this, atividades.class));
            }
        });
    }

    public void cadastrarAtividade(View v){
        startActivity(new Intent(this,atividades.class));
    }

    public void atualizarIndicadorExercicio(){
        holder.tempoExercicio.setText(preferences.ultimoTempoExercicio()+" min");
        holder.quantidadeCaloriaPerdida.setText(preferences.ultimaCaloriaGasta()+" cal");
        holder.progressBar.setProgress(preferences.somaCaloriaGasta());
        holder.textLevel.setText(preferences.getStoredString(Constantes.PROGRESSO_LEVEL));
    }

    public String getImagePath(Uri contentUri) {
        String[] campos = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, campos, null, null, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA));
        cursor.close();
        return path;
    }


    // Código botão Voltar
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //Handle the back button
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            confirmarSair();
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    private void confirmarSair() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Deseja sair da aplicação?")
                .setCancelable(false)
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void abrirMeuNivel(
            View view){
        startActivity(new Intent(this, MeuNivel.class));
    }

    private int booleanParseInt(boolean b){
       if(b) {
           return View.VISIBLE;
       }
       return View.INVISIBLE;
    }

    public void carregarGraficoHistSemanal(){

        Pie pie = AnyChart.pie();


        List<DataEntry> data = new ArrayList<>();
        data.add(new ValueDataEntry("Semana 1", 2700));
        data.add(new ValueDataEntry("Semana 2", 3200));
        data.add(new ValueDataEntry("Semana 4", 1500));
        data.add(new ValueDataEntry("Semana Atual", preferences.somaCaloriaGasta()));

        pie.setData(data);

        AnyChartView anyChartView = (AnyChartView) findViewById(R.id.any_chart_view__hist_home);
        anyChartView.setChart(pie);

        if(preferences.somaCaloriaGasta()>0){
            anyChartView.setVisibility(View.VISIBLE);
        }else{
            anyChartView.setVisibility(View.INVISIBLE);
        }
    }

    public void abrirComunidade(View v){
        startActivity(new Intent(this, PaginaComunidade.class));
    }
    public void menuHome(View v){
        startActivity(new Intent(this, Home.class));
    }
}
