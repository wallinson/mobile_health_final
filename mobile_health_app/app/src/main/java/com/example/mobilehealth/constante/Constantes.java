package com.example.mobilehealth.constante;

public class Constantes {

    public static final String EMAIL_USUARIO = "EMAIL_USUARIO";
    public static final String NOME_USUARIO = "NOME_USUARIO";
    public static final String SESSAO= "SESSAO";
    public static final String IMAGE_PERFIL= "IMAGE_PERFIL";

    public static final String INDICADORES_CONFIGURADOS = "INDICADORES_CONFIGURADOS";
    public static final String INDICADOR_EXERCICIO = "INDICADOR_EXERCICIO";
    public static final String INDICADOR_REFEICAO= "INDICADOR_REFEICAO";
    public static final String INDICADOR_MONITOR_CARDIACO = "INDICADOR_MONITOR_CARDIACO";
    public static final String INDICADOR_HISTORICO_SEMANAL = "INDICADOR_HISTORICO_SEMANAL";

    public static final String INDICADOR_MONITOR_CARDIACO_JA_SINCRONIZADO = "INDICADOR_MONITOR_CARDIACO_SINCRONIZADO";
    public static final String INDICADOR_MONITOR_CARDIACO_DT_ATUALIZACAO = "INDICADOR_MONITOR_CARDIACO_DT_ATUALIZACAO";
    public static final String DADOS_MONITOR_CARDIACO = "DADOS_MONITOR_CARDIACO";

    public static final String QUANTIDADE_EXERCICIO= "QUANTIDADE_EXERCICIO";

    public static final String LISTA_EXERCICIO= "LISTA_EXERCICIO";
    public static final String LISTA_EXERCICIO_TEMPO= "LISTA_EXERCICIO_TEMPO";
    public static final String LISTA_EXERCICIO_CALORIA= "LISTA_EXERCICIO_CALORIA";

    public static final String PROGRESSO_MAX= "PROGRESSO_MAX";
    public static final String PROGRESSO_LEVEL= "PROGRESSO_LEVEL";

    public static final String QUANTIDADE_POST= "QUANTIDADE_POST";
    public static final String INDICE_POSTAGEM= "INDICE_POSTAGEM";



}
