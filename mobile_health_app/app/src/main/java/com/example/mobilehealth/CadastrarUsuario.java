package com.example.mobilehealth;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mobilehealth.constante.Constantes;
import com.example.mobilehealth.data.Preference;

public class CadastrarUsuario extends AppCompatActivity {

    private Preference preferences;
    private ViewHolder holder = new ViewHolder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_cadastrar_usuario);

        preferences = new Preference(this);

        holder.inputNome = findViewById(R.id.inputNome);
        holder.dateNascimento = findViewById(R.id.dateNascimento);
        holder.inputAltura= findViewById(R.id.inputAltura);
        holder.inputPeso = findViewById(R.id.inputPeso);
        holder.buttonCadastrar = findViewById(R.id.buttonCadastrar);
        preferences.storeString(Constantes.PROGRESSO_MAX, "2200");
        preferences.storeString(Constantes.PROGRESSO_LEVEL, "Lv.1");

        holder.inputNome.requestFocus(1);



        holder.buttonCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(camposPreenchidos()){
                    preferences.storeString(Constantes.NOME_USUARIO, holder.inputNome.getText().toString());
                    preferences.storeBoolean(Constantes.SESSAO, true);
                    configurarTelaHome();
                    Intent telaHome = new Intent(CadastrarUsuario.this, Home.class);
                    startActivity(telaHome);

                }else{
                    Toast.makeText(CadastrarUsuario.this,"Preencha todos os campos", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    public boolean camposPreenchidos(){
        if(holder.inputNome.equals("") || holder.dateNascimento.equals("")
                || holder.inputAltura.equals("") || holder.inputPeso.equals("")){
            return false;
        }
        return true;
    }

    public void configurarTelaHome(){
            preferences.storeBoolean(Constantes.INDICADOR_EXERCICIO,true);
            preferences.storeBoolean(Constantes.INDICADOR_REFEICAO,true);
            preferences.storeBoolean(Constantes.INDICADOR_HISTORICO_SEMANAL,true);
            preferences.storeBoolean(Constantes.INDICADOR_MONITOR_CARDIACO,false);
            preferences.storeString(Constantes.QUANTIDADE_EXERCICIO, "0");
            preferences.inicializarContadorPostagem();
    }


    private static class ViewHolder{
        EditText inputNome;
        EditText  dateNascimento;
        EditText inputAltura;
        EditText inputPeso;
        Button buttonCadastrar;
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
