package com.example.mobilehealth;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.example.mobilehealth.constante.Constantes;
import com.example.mobilehealth.data.Preference;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class Postagem extends AppCompatActivity {

    private Dialog dialogImagem;
    private Preference preferences;
    private ViewHolder holder = new ViewHolder();
    static final int REQUEST_IMAGE_CAPTURE  = 1;
    String currentPhotoPath;
    Set postagem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_postagem);
        preferences = new Preference(this);
        holder.textoPostagem = findViewById(R.id.editTextoPostagem);
        holder.postagem = findViewById(R.id.imageSelecionada);
        holder.postagem.setVisibility(View.INVISIBLE);
        holder.nomeUsuario = findViewById(R.id.textNomeUsuario);
        holder.fotoPerfil = findViewById(R.id.imageFotoPerfil);
        findViewById(R.id.progressBar).setVisibility(View.INVISIBLE);
        findViewById(R.id.textLevel).setVisibility(View.INVISIBLE);
        holder.nomeUsuario.setText(preferences.getStoredString(Constantes.NOME_USUARIO));

        ActionBar supportActionBar = getSupportActionBar();
        supportActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#F80C0C")));

        postagem = new HashSet();
        carregarImagem();

    }

    public void addImagemPostagem(View v) {
        dialogImagem = new Dialog(Postagem.this);
        dialogImagem.setContentView(R.layout.modal_selecionar_imagem);
        dialogImagem.show();
    }

    public void selecionarImagemCamera(View v) {
        abrirCamera();
        dialogImagem.hide();
        holder.postagem.setVisibility(View.VISIBLE);
    }

    public void selecionarImagemGaleria(View v) {
        dialogImagem.hide();
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(Intent.createChooser(i, "Selecione uma imagem"), 123);
        holder.postagem.setVisibility(View.VISIBLE);
        dialogImagem.cancel();
    }

    public void postar(View view) {
        String textoPostagem = holder.textoPostagem.getText().toString();
        postagem.add(textoPostagem);
        preferences.cadastrarPostagem(postagem);
        startActivity(new Intent(this, PaginaComunidade.class));
        finish();

    }

    public class ViewHolder{
        EditText textoPostagem;
        ImageView postagem;
        ImageView fotoPerfil;
        ProgressBar progressBar;
        TextView textLevel;
        TextView nomeUsuario;
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = ""+System.currentTimeMillis();
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void abrirCamera() {
        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePhotoIntent.resolveActivity(getPackageManager()) != null) {

            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created

            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(Postagem.this,"androidx.core.content.fileprovide", photoFile);
                takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePhotoIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri imagemSelecionada = null;
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            postagem.add(currentPhotoPath);
            holder.postagem.setImageBitmap(imageBitmap);
        }else if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 123) {
                imagemSelecionada = data.getData();
                postagem.add(getImagePath(imagemSelecionada));
                holder.postagem.setImageURI(imagemSelecionada);
            }
        }
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(currentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    public String getImagePath(Uri contentUri) {
        String[] campos = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, campos, null, null, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA));
        cursor.close();
        return path;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void carregarImagem() {
        if (preferences.getStoredString(Constantes.IMAGE_PERFIL).length() > 0) {
            Uri uri = Uri.fromFile(new File(preferences.getStoredString(Constantes.IMAGE_PERFIL)));
            holder.fotoPerfil.setImageURI(uri);
        }
    }

    public void abrirComunidade(View v){
        startActivity(new Intent(this, PaginaComunidade.class));
    }
    public void menuHome(View v){
        startActivity(new Intent(this, Home.class));
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
