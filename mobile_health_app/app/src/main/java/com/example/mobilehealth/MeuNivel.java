package com.example.mobilehealth;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mobilehealth.constante.Constantes;
import com.example.mobilehealth.data.Preference;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;

public class MeuNivel extends AppCompatActivity {

    private Preference preferences;
    private ViewHolder holder = new ViewHolder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_meu_nivel);
        preferences = new Preference(this);
        holder.pontosRestantes = findViewById(R.id.textRestante);
        holder.progressCircle = findViewById(R.id.progressCircle);
        holder.progressBar = findViewById(R.id.progressBar);
        findViewById(R.id.textLevel).setVisibility(View.INVISIBLE);

        holder.textLevel = findViewById(R.id.textLevelAtual);
        holder.textMeusPontos = findViewById(R.id.textMeusPontos);

        holder.textLevel.setText(preferences.getStoredString(Constantes.PROGRESSO_LEVEL));
        holder.textMeusPontos.setText(preferences.somaCaloriaGasta() +" / " + preferences.getStoredString(Constantes.PROGRESSO_MAX));


        holder.progressBar.setVisibility(View.INVISIBLE);

        holder.progressCircle.setMax(Integer.valueOf(preferences.getStoredString(Constantes.PROGRESSO_MAX)));
        holder.progressCircle.setProgress(Integer.valueOf(preferences.somaCaloriaGasta()));

        holder.pontosRestantes.setText(" Faltam "+calcularDiferenca()+" pontos para o próximo nível.");
        holder.imageFotoPerfil = findViewById(R.id.imageFotoPerfil);
        carregarImagem();
        findViewById(R.id.imageEdit).setVisibility(View.INVISIBLE);




    }

    public int calcularDiferenca(){
        return Integer.valueOf(preferences.getStoredString(Constantes.PROGRESSO_MAX)) - Integer.valueOf(preferences.somaCaloriaGasta());
    }

    public void carregarImagem() {
        if (preferences.getStoredString(Constantes.IMAGE_PERFIL).length() > 0) {
            Uri uri = Uri.fromFile(new File(preferences.getStoredString(Constantes.IMAGE_PERFIL)));
            holder.imageFotoPerfil.setImageURI(uri);
        }
    }

    private static class ViewHolder {
        TextView pontosRestantes;
        ProgressBar progressBar;
        ProgressBar progressCircle;
        TextView textLevel;
        TextView textMeusPontos;
        CircleImageView imageFotoPerfil;
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    public void abrirComunidade(View v){
        startActivity(new Intent(this, PaginaComunidade.class));
    }
    public void menuHome(View v){
        startActivity(new Intent(this, Home.class));
    }
}
