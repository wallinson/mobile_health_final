package com.example.mobilehealth;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mobilehealth.constante.Constantes;
import com.example.mobilehealth.data.Preference;

public class Entrar extends AppCompatActivity {

    private Preference preferences;
    private ViewHolder holder = new ViewHolder();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_entrar);

        preferences = new Preference(this);

        holder.email = findViewById(R.id.inputEmail);
        holder.senha = findViewById(R.id.inputSenha);

        holder.botaoEntrar = findViewById(R.id.buttonEntrar);
        holder.botaoEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(camposPreenchidos()){
                    preferences.storeString(Constantes.EMAIL_USUARIO, holder.email.getText().toString());
                    Intent cadastro = new Intent(Entrar.this, CadastrarUsuario.class);
                    startActivity(cadastro);
                }else{
                    Toast.makeText(Entrar.this,"Campo Email e/ou Senha em branco!!!", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    public boolean camposPreenchidos(){
        if(holder.email.getText().length() == 0 || holder.senha.getText().length() ==0){
            return false;
        }
        return true;
    }

    private static class ViewHolder{
        EditText email;
        EditText senha;
        Button botaoEntrar;
    }
}
